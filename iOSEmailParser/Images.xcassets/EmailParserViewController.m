//
//  ViewController.m
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/1/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import "EmailParserViewController.h"
#import "EmailParserFactory.h"
#import "Email.h"
#import "EmailBodyModel.h"
#import "EmailDisplayTableViewCell.h"

@interface EmailParserViewController ()

@property (nonatomic, strong) Email *email;

@end

@implementation EmailParserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    EmailParserFactory *emailParserFactory = [EmailParserFactory new];
    self.email = [emailParserFactory buildEmailFromFile:@"savedEmail"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// MARK: UITableViewDelegate Protocol
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger sectionEmailHeaders = 1;
    NSInteger sectionEmailBody = 1;
    NSInteger numberOfSections = sectionEmailHeaders + sectionEmailBody;
    
    return numberOfSections;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EmailDisplayTableViewCell *selectedCell = (EmailDisplayTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

// MARK: UITableViewDataSource Protocol
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EmailDisplayTableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EmailDisplayTableViewCell class])];
    
    NSString *headerText = @"";
    NSString *contentText = @"";
    NSString *contentType = @"text/plain";

    switch (indexPath.section) {
        case 0: {
            
            NSArray *headersKeys = self.email.headersDictionary.allKeys;
            NSArray *headersValues = self.email.headersDictionary.allValues;

            headerText = headersKeys[indexPath.row];
            contentText = headersValues[indexPath.row];
            
        }
            break;
            
        case 1: {
            EmailBodyModel *bodyModel = self.email.bodyContentArray[indexPath.row];
            headerText = [NSString stringWithFormat:@"Body %li: %@", (long)indexPath.row, bodyModel.contentType];
            contentType = bodyModel.contentType;
            contentText = bodyModel.contentText;
        }
            break;
            
        default:
            NSAssert(NO, @"Unaccounted for section");
            break;
    }
    
    [tableCell configureWithHeaderString:headerText contentString:contentText contentType:contentType];
    
    return tableCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // calling directly to self is bad?
    EmailDisplayTableViewCell *currentCell = (EmailDisplayTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    CGFloat heightForRowAtIndexPath = [currentCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    return heightForRowAtIndexPath;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRowsInSection = 0;
    
    switch (section) {
            
        case 0: // headers
            numberOfRowsInSection = self.email.headersDictionary.allKeys.count;
            break;
            
        case 1: // body
            numberOfRowsInSection = self.email.bodyContentArray.count;
            break;
            
        default:
            break;
    }
    
    return numberOfRowsInSection;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *contentView = [UIView new];
    contentView.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 44);
    contentView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:1.0f alpha:1.0f];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(10, 0, CGRectGetWidth(tableView.frame) - 20, 44);
    NSString *title = @"";
    
    switch (section) {
        case 0: // Headers
            title = @"Headers";
            break;
            
        case 1:
            title = @"Body Content";
            break;
            
        default:
            break;
    }
    
    titleLabel.text = title;
    
    [contentView addSubview:titleLabel];
    
    return contentView;
}

@end
