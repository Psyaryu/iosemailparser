//
//  ViewController.h
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/1/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailParserViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
