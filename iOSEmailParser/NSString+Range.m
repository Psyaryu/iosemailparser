//
//  NSString+Range.m
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/8/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import "NSString+Range.h"

@implementation NSString (Range)

- (NSRange)range {
    return NSMakeRange(0, [self length]);
}

@end
