//
//  Email.h
//  iOSEmailParser
//
//  Created by Asa on 11/15/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

@import Foundation;

@interface Email : NSObject

@property (nonatomic, strong) NSString *rawEmail;
@property (nonatomic, strong) NSDictionary *headersDictionary;
@property (nonatomic, strong) NSArray *bodyContentArray;

@property (nonatomic, strong) NSString *contentTypeBoundary;
@property (nonatomic, strong) NSString *contentTypeCharSet;
@property (nonatomic, strong) NSString *contentType;

@end
