//
//  main.m
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/1/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
