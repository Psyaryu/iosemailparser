//
//  EmailParserRegExFactory.h
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/8/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmailParserRegExFactory : NSObject

+ (NSRegularExpression *)buildHeaderFieldRegEx;
+ (NSRegularExpression *)buildMimeVersionRegEx;
+ (NSRegularExpression *)buildGlobalHeaderBodySeparatorRegEx;
+ (NSRegularExpression *)buildBodyContentHeaderBodySeparatorRegEx;
+ (NSRegularExpression *)buildQuottingPrintableDelimiterRegEx;

@end
