//
//  Email.m
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/1/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import "EmailParserFactory.h"
#import "EmailParserRegExFactory.h"
#import "NSString+Range.h"
#import "EmailBodyModel.h"
#import "Email.h"

@interface EmailParserFactory ()

// need to keep state for this object
@property (nonatomic, strong) Email *currentBuildingEmail;

@end

@implementation EmailParserFactory

- (Email *)buildEmailFromFile:(NSString *)fileName {
    
    self.currentBuildingEmail = [Email new];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"txt"];
    NSString *contentOfFileString = [NSString stringWithContentsOfFile:path encoding:NSStringEncodingConversionExternalRepresentation error:nil];
    
    self.currentBuildingEmail.rawEmail = contentOfFileString;
    
    NSTextCheckingResult *result = [[EmailParserRegExFactory buildGlobalHeaderBodySeparatorRegEx] firstMatchInString:self.currentBuildingEmail.rawEmail options:0 range:self.currentBuildingEmail.rawEmail.range];
    
    NSString *headersString = [self.currentBuildingEmail.rawEmail substringToIndex:result.range.location];
    NSString *bodyString = [self.currentBuildingEmail.rawEmail substringFromIndex:result.range.location];
    
    self.currentBuildingEmail.headersDictionary = [self generateHeaderDictionaryFromHeadersString:headersString];
    [self extractContentTypeDataFromHeadersDictionary:self.currentBuildingEmail.headersDictionary];
    
    self.currentBuildingEmail.bodyContentArray = [self generateBodyModelArrayFromBodyString:bodyString];
    
    return self.currentBuildingEmail;
}

- (NSDictionary *)generateHeaderDictionaryFromHeadersString:(NSString *)headersString {
    
    NSMutableDictionary *dictionaryOfParsedHeaders = [NSMutableDictionary new];
    
    NSRange headerStringRange = NSMakeRange(0, headersString.length);
    
    NSArray *matches = [[EmailParserRegExFactory buildHeaderFieldRegEx] matchesInString:headersString options:0 range:headerStringRange];
    
    [matches enumerateObjectsUsingBlock:^(NSTextCheckingResult *result, NSUInteger idx, BOOL *stop) {
        
        NSString *headerString = [headersString substringWithRange:result.range];
        NSRange headerDelimiterRange = [headerString rangeOfString:@":"];
                
        NSString *title = [headerString substringToIndex:headerDelimiterRange.location];
        NSString *body = [headerString substringFromIndex:headerDelimiterRange.location + headerDelimiterRange.length];
        
        body = [body stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        body = [self unFold:body];

        dictionaryOfParsedHeaders[title] = body;
        
    }];
    
    return  [NSDictionary dictionaryWithDictionary:dictionaryOfParsedHeaders];
}

- (NSArray *)generateBodyModelArrayFromBodyString:(NSString *)bodyString {
    
    NSMutableArray *parsedBodyElements = [NSMutableArray new];
    NSRegularExpression *bodyContentSeparatorRegEx = [[NSRegularExpression alloc] initWithPattern:self.currentBuildingEmail.contentTypeBoundary options:0 error:nil];
    NSArray *bodySeparatorTextResults = [bodyContentSeparatorRegEx matchesInString:bodyString options:0 range:bodyString.range];
    
    [bodySeparatorTextResults enumerateObjectsUsingBlock:^(NSTextCheckingResult *textResult, NSUInteger idx, BOOL *stop) {
        
        NSTextCheckingResult *contentBeginningTextResult = textResult;
        NSTextCheckingResult *contentEndingTextResult = bodySeparatorTextResults[idx + 1];
        
        NSUInteger location = ( contentBeginningTextResult.range.location + contentBeginningTextResult.range.length );
        NSUInteger length = contentEndingTextResult.range.location - location;
        
        NSRange rangeOfBody = NSMakeRange(location, length);
        
        NSString *contentBodyString = [bodyString substringWithRange:rangeOfBody];
        
        EmailBodyModel *emailBodyModel = [self generateBodyModelFromBodyString:contentBodyString];
        
        [parsedBodyElements addObject:emailBodyModel];
        
        *stop = [contentEndingTextResult isEqual:bodySeparatorTextResults.lastObject];
        
    }];
    
    
    return [NSArray arrayWithArray:parsedBodyElements];
    
}

- (EmailBodyModel *)generateBodyModelFromBodyString:(NSString *)bodyString {
    
    EmailBodyModel *emailBodyModel = [EmailBodyModel new];
    
    NSTextCheckingResult *separateBodyHeaderFromBodyContent = [[EmailParserRegExFactory buildBodyContentHeaderBodySeparatorRegEx] firstMatchInString:bodyString options:0 range:bodyString.range];
    
    NSRange bodyContentHeadersRange = NSMakeRange(0, separateBodyHeaderFromBodyContent.range.location);
    
    NSInteger bodyContentRangeLocation = separateBodyHeaderFromBodyContent.range.location + separateBodyHeaderFromBodyContent.range.length;
    NSRange bodyContentRange = NSMakeRange(bodyContentRangeLocation, bodyString.range.length - bodyContentRangeLocation);
    
    NSString *bodyContentHeaders = [bodyString substringWithRange:bodyContentHeadersRange];
    NSString *bodyContent = [bodyString substringWithRange:bodyContentRange];
    
    NSDictionary *bodyHeadersDictionary = [self generateHeaderDictionaryFromBodyContentHeaderString:bodyContentHeaders];
    
    emailBodyModel.contentCharSet = bodyHeadersDictionary[@"charset"];
    emailBodyModel.contentType = bodyHeadersDictionary[@"Content-Type"];
    emailBodyModel.contentTransferEncoding = bodyHeadersDictionary[@"Content-Transfer-Encoding"];
    emailBodyModel.contentText = [self gernerateReadableBodyFromBodyContentString:bodyContent usingTransferEncodingType:bodyHeadersDictionary[@"Content-Transfer-Encoding"]];
    
    return emailBodyModel;
}

- (NSDictionary *)generateHeaderDictionaryFromBodyContentHeaderString:(NSString *)bodyContentHeader {
    
    NSArray *headersOfBodies = [[EmailParserRegExFactory buildHeaderFieldRegEx] matchesInString:bodyContentHeader options:0 range:bodyContentHeader.range];
    
    NSMutableDictionary *dictionaryOfHeaders = [NSMutableDictionary new];
    [headersOfBodies enumerateObjectsUsingBlock:^(NSTextCheckingResult *headerFieldTextResult, NSUInteger idx, BOOL *stop) {
        
        NSString *foldedHeader = [bodyContentHeader substringWithRange:headerFieldTextResult.range];
        NSString *unFoldedHeader = [self unFold:foldedHeader];
        
        NSArray *brokenHeader = [unFoldedHeader componentsSeparatedByString:@";"];
        [brokenHeader enumerateObjectsUsingBlock:^(NSString *header, NSUInteger idx, BOOL *stop) {
            NSString *delimiter;
            if ([header rangeOfString:@":"].location != NSNotFound) {
                delimiter = @":";
            } else if ([header rangeOfString:@"="].location != NSNotFound) {
                delimiter = @"=";
            } else {
                NSAssert(NO, @"Un accounted for property delimiter");
            }
            
            NSArray *headerComponents = [header componentsSeparatedByString:delimiter];
            NSString *headerKeyStrippedOfWhiteSpace = [headerComponents.firstObject stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *headerValueStrippedOfWhiteSpace = [headerComponents.lastObject stringByReplacingOccurrencesOfString:@" " withString:@""];

            dictionaryOfHeaders[headerKeyStrippedOfWhiteSpace] = headerValueStrippedOfWhiteSpace;
            
        }];

    }];
    
    return [NSDictionary dictionaryWithDictionary:dictionaryOfHeaders];
}

- (NSString *)gernerateReadableBodyFromBodyContentString:(NSString *)bodyContent usingTransferEncodingType:(NSString*)transferEncodingType {
    
    NSString *parsedBodyContent;
    
    if ([transferEncodingType isEqualToString:@"quoted-printable"]) {
        
        NSTextCheckingResult *bodyContentDelimiterTextResults = [[EmailParserRegExFactory buildQuottingPrintableDelimiterRegEx]firstMatchInString:bodyContent options:0 range:bodyContent.range];
        
        NSString *delimiterText = [bodyContent substringWithRange:bodyContentDelimiterTextResults.range];
        
        parsedBodyContent = [bodyContent stringByReplacingOccurrencesOfString:delimiterText withString:@""];
        
    } else {
        NSAssert(NO, @"Unaccounted for transfer encoding type");
    }
    
    return parsedBodyContent;
}

- (NSString *)unFold:(NSString *)fieldBody {
    
    // folded bodies generally have
    // new line characters (\n)
    // carriage return (\r)
    // tab (\t)
    // fake tab of 4 spaces (    )
    
    NSString *body = fieldBody;
    body = [body stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    body = [body stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    body = [body stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    body = [body stringByReplacingOccurrencesOfString:@"    " withString:@""];
    return body;
}

- (void)extractContentTypeDataFromHeadersDictionary:(NSDictionary *)headersDictionary {
    
    NSString *contentTypeData = headersDictionary[@"Content-Type"];
    NSArray *contentSubData = [contentTypeData componentsSeparatedByString:@";"];
    
    self.currentBuildingEmail.contentType = contentSubData.firstObject;
    
    NSArray *boundaryCompoenents = [contentSubData[1] componentsSeparatedByString:@"\""];
    self.currentBuildingEmail.contentTypeBoundary = boundaryCompoenents[1];
    
    NSArray *charSetComponets = [contentSubData[2] componentsSeparatedByString:@"="];
    self.currentBuildingEmail.contentTypeCharSet = charSetComponets.lastObject;
}

@end
