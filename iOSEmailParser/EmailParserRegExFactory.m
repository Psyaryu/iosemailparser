//
//  EmailParserRegExFactory.m
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/8/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import "EmailParserRegExFactory.h"

@implementation EmailParserRegExFactory


// Header Folder/Unfolded
// ! to ~ excluding colon : HTAB or ! to ~ \r\n
+ (NSRegularExpression *)buildHeaderFieldRegEx {
    
    NSString *headerFieldPattern = @"^[\041-\176^\072]*:[\011|\032-\176]*(\r?\n[\t\f ][\032-\176]*)*$";
    
    NSRegularExpression *headersRegularExpression = [[NSRegularExpression alloc] initWithPattern:headerFieldPattern options:NSRegularExpressionAnchorsMatchLines error:nil];
    
    return headersRegularExpression;

}

+ (NSRegularExpression *)buildMimeVersionRegEx {
    
    NSString *mimeVersionPattern = @"\r?\nMime-Version[\\s\\S]*?\r?\n";
    
    NSRegularExpression *mimeVersionRegex = [[NSRegularExpression alloc] initWithPattern:mimeVersionPattern options:NSRegularExpressionCaseInsensitive error:nil];
    
    return mimeVersionRegex;
    
}

+ (NSRegularExpression *)buildGlobalHeaderBodySeparatorRegEx {
    
    NSString *headerBodySeparatorPattern = @"\n\n\n?";
    
    NSRegularExpression *headerBodySeparatorRegEx = [[NSRegularExpression alloc] initWithPattern:headerBodySeparatorPattern options:NSRegularExpressionCaseInsensitive error:nil];
    
    return headerBodySeparatorRegEx;
    
}

+ (NSRegularExpression *)buildBodyContentHeaderBodySeparatorRegEx {
    NSString *bodyContentHeaderBodySeparato = @"\n\n";
    
    NSRegularExpression *headerBodySeparatorRegEx = [[NSRegularExpression alloc] initWithPattern:bodyContentHeaderBodySeparato options:NSRegularExpressionCaseInsensitive error:nil];
    
    return headerBodySeparatorRegEx;
}

+ (NSRegularExpression *)buildQuottingPrintableDelimiterRegEx {
    NSString *quottingPrintableDelimiterPattern = @"=\r?\n";
    
    NSRegularExpression *quottingPrintableDelimiterRegEx = [[NSRegularExpression alloc] initWithPattern:quottingPrintableDelimiterPattern options:NSRegularExpressionCaseInsensitive error:nil];
    
    return quottingPrintableDelimiterRegEx;
}

@end
