//
//  EmailDisplayTableViewCell.h
//  iOSEmailParser
//
//  Created by Asa on 11/15/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailDisplayTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *headerLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;

- (void)configureWithHeaderString:(NSString *)headerString contentString:(NSString *)contentString contentType:(NSString *)type;

@end
