//
//  EmailBodyModel.m
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/8/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import "EmailBodyModel.h"

@implementation EmailBodyModel

- (NSString *)description {
    NSString *defaultDescription = [super description];
    NSString *newDescription = [NSString stringWithFormat:@"%@\n\tContent-Type = %@\n\tChar Set = %@\n\tContent-Transfer-Encoding = %@", defaultDescription, self.contentType, self.contentCharSet, self.contentTransferEncoding];
    
    return newDescription;
}

@end
