//
//  Email.h
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/1/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Email;

@interface EmailParserFactory : NSObject

- (Email *)buildEmailFromFile:(NSString *)fileName;

@end
