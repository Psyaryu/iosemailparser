//
//  EmailBodyModel.h
//  iOSEmailParser
//
//  Created by A'sa Dickens on 11/8/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmailBodyModel : NSObject

@property (nonatomic, strong) NSString *contentType;
@property (nonatomic, strong) NSString *contentCharSet;
@property (nonatomic, strong) NSString *contentText;
@property (nonatomic, strong) NSString *contentTransferEncoding;

@end
