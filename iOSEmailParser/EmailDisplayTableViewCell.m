//
//  EmailDisplayTableViewCell.m
//  iOSEmailParser
//
//  Created by Asa on 11/15/14.
//  Copyright (c) 2014 A'sa Dickens. All rights reserved.
//

#import "EmailDisplayTableViewCell.h"

@implementation EmailDisplayTableViewCell

- (void)configureWithHeaderString:(NSString *)headerString contentString:(NSString *)contentString contentType:(NSString *)type {

    NSAttributedString *attributedHeader = [[NSAttributedString alloc] initWithString:headerString];
    NSAttributedString *attributedContent = [[NSAttributedString alloc] initWithString:contentString];

    if ([type isEqualToString:@"text/plain"]) {
        
    } else if ([type isEqualToString:@"text/html"]) {
        
        NSDictionary *attributes = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                                      NSCharacterEncodingDocumentAttribute : @(NSUTF8StringEncoding)
                                      };
        NSData *htmlData = [contentString dataUsingEncoding:NSUTF8StringEncoding];
        attributedContent = [[NSMutableAttributedString alloc] initWithData:htmlData options:attributes documentAttributes:nil error:nil];
        
    } else {
        NSAssert(NO, @"Unaccounted for textType");
    }
    
    self.headerLabel.attributedText = attributedHeader;
    self.contentLabel.attributedText = attributedContent;
    
}

@end
